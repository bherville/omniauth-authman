# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'omniauth/authman/version'

Gem::Specification.new do |spec|
  spec.name          = "omniauth-authman"
  spec.version       = Omniauth::Authman::VERSION
  spec.authors       = ["Brad Herring"]
  spec.email         = ["brad@bherville.com"]

  spec.summary       = %q{OmniAuth strategy gem for AuthMan}
  spec.description   = %q{OmniAuth strategy gem for AuthMan}
  spec.homepage      = 'https://bitbucket.org/bherring/authman'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'omniauth-oauth2'

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
end
