require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class AuthMan < OmniAuth::Strategies::OAuth2
      # change the class name and the :name option to match your application name
      option :name, :auth_man

      option :client_options, {
                                :site => Omniauth::Authman::URL,
                                :authorize_url => '/oauth/authorize'
                            }

      uid { raw_info['id'] }

      info do
        {
            :email            => raw_info['email'],
            :fname            => raw_info['fname'],
            :lname            => raw_info['lname'],
            :time_zone        => raw_info['time_zone'],
            :my_account       => "#{Omniauth::Authman::URL}/user",
            :enable_gravatar  => raw_info['enable_gravatar']
        }
      end

      def raw_info
        @raw_info ||= access_token.get('/api/me.json').parsed
      end
    end
  end
end